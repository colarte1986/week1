$(function () {
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
  				interval: 2000
  			});
  			$('#informacion').on('show.bs.modal', function(e){
  				console.log('el modal información se está mostrando');

  				$('#contactoBtn').removeClass('btn-outline-success');
  				$('#contactoBtn').addClass('btn-primary');
  				$('#contactoBtn').prop('disabled', true);
  			});
  			$('#informacion').on('shown.bs.modal', function(e){
  				console.log('el modal información se mostró')
  			});
  			$('#informacion').on('hide.bs.modal', function(e){
  				console.log('el modal información se está cerrando')
  			});
  			$('#informacion').on('hidden.bs.modal', function(e){
  				console.log('el modal información se cerró');
  				$('#contactoBtn').prop('disabled', false);
  				$('#contactoBtn').removeClass('btn-primary');
  				$('#contactoBtn').addClass('btn-outline-success');
  			});
		})